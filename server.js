/**
 * Created by Max.K on 26.07.2018.
 */
var http = require('http').Server();
var io = require('socket.io')(http);
var Redis = require('ioredis');

var redis = new Redis();

redis.subscribe('news-action');

redis.on('message', function (channel, message) {
    console.log(message);
    console.log(channel);
    message = JSON.parse(message);
    socket.emit(channel + ':' + message.event, message.data);
});

http.listen(3000, function () {
    console.log('Listening on port 3000')
});