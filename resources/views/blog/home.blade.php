@extends('layouts.app')

@section('content')
    <div class="container">
        @forelse($articles as $article)
            <div class="row">
                <div class="col-sm-12">
                    <h1><a href="{{route('article', $article->slug)}}">{{$article->title}}</a></h1>
                    <p>{!! $article->description_short !!}</p>
                    <hr>
                </div>
            </div>
        @empty
            <h2 class="text-center">Пусто</h2>
        @endforelse

    </div>
@endsection
