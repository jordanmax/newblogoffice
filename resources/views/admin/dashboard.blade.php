@extends('admin.layouts.app_admin')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="jumbotron text-center">
                    <p><span class="label">
                           <button type="button" class="btn btn-primary btn-lg btn-block">Категорий {{$count_categories}}</button>
                        </span>
                    </p>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="jumbotron text-center">
                    <p><span class="label">
                            <button type="button" class="btn btn-primary btn-lg btn-block">  Новостей {{$count_articles}} </button>
                        </span>
                    </p>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="jumbotron text-center">
                    <p><span class="label">
                            <button type="button" class="btn btn-primary btn-lg btn-block"> Пользователей </button>
                        </span>
                    </p>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="jumbotron text-center">
                    <p><span class="label">
                            <button type="button" class="btn btn-primary btn-lg btn-block">Сегодня</button>
                        </span>
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <a class="btn btn-block btn-default" href="{{route('admin.category.create')}}">Создать категорию </a>
                @foreach($categories as $category)
                <a class="list-group-item" href="{{route('admin.category.edit', $category)}}">
                    <h4 class="list-group-item-heading">{{$category->title}}</h4>
                    <p class="list-group-item-text">
                        Количество новостей в категории: <b>{{$category->articles()->count()}}</b>
                    </p>
                </a>
                @endforeach
            </div>
            <div class="col-sm-6">
                <a class="btn btn-block btn-default" href="{{route('admin.article.create')}}">Создать новость</a>
                @foreach($articles as $article)
                <a class="list-group-item" href="{{route('admin.article.edit', $article)}}">
                    <h4 class="list-group-item-heading">{{$article->title}}</h4>
                    <p class="list-group-item-text">Категория: {{$article->categories()->pluck('title')->implode(', ')}}</p>
                </a>
                @endforeach
            </div>
        </div>
    </div>
@endsection