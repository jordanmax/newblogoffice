<label for="">Status</label>
<select class="form-control" name="published">
    @if (isset($article->id))
        <option value="0" @if ($article->published == 0) selected="" @endif> No published</option>
        <option value="1" @if ($article->published == 1) selected="" @endif> Published</option>
    @else
        <option value="0">No published</option>
        <option value="1">Published</option>
    @endif
</select>

<label for=""> Имя</label>
<input type="text" class="form-control" name="title" placeholder="Название категории" value="{{$article->title or ""}}" required>

<label for=""> Slug</label>
<input type="text" class="form-control" name="slug" placeholder="Automatic slug" value="{{$article->slug or ""}}" readonly="">

<label for=""> Родительская категория</label>
<select class="form-control" name="categories[]" multiple="">
    @include('admin.articles.partials.categories', ['categories' => $categories])
</select>

<label for="">Описание новости</label>
<textarea class="form-control" id="description_short" name="description_short">{{$article->description_short or ""}}</textarea>

<label for="">Полное описание</label>
<textarea class="form-control" id="description" name="description">{{$article->description or ""}}</textarea>
<hr>
<label for="">Meta title</label>
<input type="text" class="form-control" name="meta_title" placeholder="Meta Title" value="{{$article->meta_title or ""}}">

<label for="">Meta desc</label>
<input type="text" class="form-control" name="meta_description" placeholder="Meta desc" value="{{$article->meta_description or ""}}">

<label for="">Meta teg</label>
<input type="text" class="form-control" name="meta_keyword" placeholder="Meta Title" value="{{$article->meta_keyword or ""}}">
<hr>

<input class="btn btn-primary" type="submit" value="Save">