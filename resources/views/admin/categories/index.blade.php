@extends('admin.layouts.app_admin')

@section('content')
    <div class="container">
        @component('admin.components.breadcrump')
            @slot('title') Список категорий @endslot
            @slot('parent') Главная @endslot
            @slot('active') Категории @endslot
        @endcomponent
        <hr>
        <a href="{{route('admin.category.create')}}" class="btn btn-primary pull-right"><i class="fa fa-plus-square-o"></i>Создать Категорию</a>
        <table class="table table-striped">
            <thead>
                <th>Имя</th>
                <th>Опубликовано</th>
                <th class="text-right">Действие</th>
            </thead>
            <tbody>
                @forelse($categories as $category)
                    <tr>
                        <td>{{$category->title}}</td>
                        <td>{{$category->published}}</td>
                        <td class="text-right">
                            <form onsubmit="if(confirm('Delete')){return true}else {return false}" action="{{route('admin.category.destroy', $category)}}" method="post">
                                <input type="hidden" name="_method" value="DELETE">
                                {{csrf_field()}}
                                <a class="btn btn-default" href="{{route('admin.category.edit', $category)}}"><i class="fas fa-edit"></i></a>
                                <button type="submit" class="btn"><i class="far fa-trash-alt"></i></button>
                            </form>
                            {{--<a href="{{route('admin.category.edit', ['id'=>$category])}}"><i class="fa fa-pencil-square-o"></i> </a>--}}
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="3" class="text-center"><h2>No info</h2></td>
                    </tr>
                @endforelse
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="3">
                        <ul class="pagination pull-right">
                            {{$categories->links()}}
                        </ul>
                    </td>
                </tr>
            </tfoot>
        </table>
    </div>
@endsection