<label for="">Статус</label>
<select class="form-control" name="published">
    @if (isset($category->id))
        <option value="0" @if ($category->published == 0) selected="" @endif> Не опубликовувать</option>
        <option value="1" @if ($category->published == 1) selected="" @endif> Опубликовать</option>
    @else
        <option value="0">Не опубликовувать</option>
        <option value="1">Опубликовать</option>
    @endif
</select>

<label for=""> Имя</label>
<input type="text" class="form-control" name="title" placeholder="Имя категории" value="{{$category->title or ""}}" required>

<label for=""> Slug</label>
<input type="text" class="form-control" name="slug" placeholder="Automatic slug" value="{{$category->slug or ""}}" readonly="">

<label for="">Доччерняя категория</label>
<select class="form-control" name="parent_id">
    <option value="0">-- Самостоятельная категория --</option>
    @include('admin.categories.partials.categories', ['categories' => $categories])
</select>

<hr>

<input class="btn btn-primary" type="submit" value="Добавить">