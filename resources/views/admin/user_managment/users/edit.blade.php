@extends('admin.layouts.app_admin')

@section('content')
    <div class="container">
        @component('admin.components.breadcrump')
            @slot('title') Редактировать пользователя @endslot
            @slot('parent') Главная @endslot
            @slot('active') Пользователи @endslot
        @endcomponent
        <hr>
        <form class="form-horizontal" action="{{route('admin.user_managment.user.update', $user)}}" method="post">
            <input type="hidden" name="_method" value="put">
            {{csrf_field()}}
            @include('admin.user_managment.users.partials.form')
        </form>
    </div>
@endsection