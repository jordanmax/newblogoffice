<?php

namespace App\Http\Controllers;

use App\Events\NewEvent;
use Illuminate\Http\Request;

class VueController extends Controller
{
    public function index(){
        $url_data = [
            array(
                'title' => 'Max',
                'url' => 'text_vue'
            ),
            array(
                'title' => 'Ivan',
                'url' => 'vue_test'
            )
        ];
        return view('vue.prop', [
            'url_data' => $url_data
        ]);
    }
    public function getJson(){


        return [
            array(
                'title' => 'Test',
                'url' => 'text_vue'
            ),
            array(
                'title' => 'Test',
                'url' => 'vue_test'
            )
        ];
    }
    public function chartData(){
        return [
            'labels' => ['1','2','3','4'],
            'datasets' => array([
                'label' => 'Test',
                'backgroundColor' => '#f26202',
                'data' => [18423,12356,21345,8576],
            ])
        ];
    }

    public function newEvent(Request $request){
        $result = [
            'labels' => ['1','2','3','4'],
            'datasets' => array([
                'label' => 'Test',
                'backgroundColor' => '#f26202',
                'data' => [18423,12356,21345,8576],
            ])
        ];
        if ($request->has('label')) {
            $result['labels'][] = $request->input('label');
            $result['datasets'][0]['data'][] = (integer)$request->input('sale');
            if ($request->has('realtime')) {
                if (filter_var($request->input('realtime'), FILTER_VALIDATE_BOOLEAN)) {
                    event(new NewEvent($result));
                }
            }
        }

        return $result;
    }
}
